package com.abc.itunes.common;

import com.abc.itunes.R;

/**
 * Created by Alberto on 20/07/17.
 */

public class Constants {

    public static final String _URL = "https://itunes.apple.com/us/rss/topfreeapplications/limit=20/json";

    public static final String _HTTP_STRING_CONTENT_TYPE = "Content-Type";
    public static final String _HTTP_STRING_APPLICATION = "application/json;";

    public static final String _JSON_KEY_FEED = "feed";
    public static final String _JSON_KEY_ENTRY = "entry";

    public static final String _JSON_KEY_NAME = "im:name";
    public static final String _JSON_KEY_IMAGE = "im:image";
    public static final String _JSON_KEY_SUMMARY = "summary";
    public static final String _JSON_KEY_PRICE = "im:price";

    public static final String _JSON_KEY_LABEL = "label";
    public static final String _JSON_KEY_ATTRIBUTES = "attributes";
    public static final String _JSON_KEY_AMOUNT = "amount";

    public static final String _CURRENCY = "$";

    public static final String _STR_ERROR_SERVER = Utils.getResString(R.string.str_error_service);
    public static final String _STR_TITLE_ERROR = Utils.getResString(R.string.str_title_error);
    public static final String _STR_ERROR_NETWORK = Utils.getResString(R.string.str_error_network);

    public static final String _STR_DONE = Utils.getResString(R.string.str_done);
    public static final String _STR_LOADING = Utils.getResString(R.string.str_loading);
    public static final String _STR_WAIT = Utils.getResString(R.string.str_wait);

    public enum StatusResponse {
        Ok, Error, Null, JSONException, Offline
    }

    public enum Operation {
        GetApps
    }

}
