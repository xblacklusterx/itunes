package com.abc.itunes.common;

import android.app.Application;
import android.content.Context;

import com.abc.itunes.models.AppStore;

/**
 * Created by Alberto on 20/07/17.
 */

public class AppSession extends Application {

    public static Context appContext;

    public static AppStore appStore;

    @Override
    public void onCreate() {
        super.onCreate();

        initApplication();
    }

    private void initApplication() {
        appContext = this;
    }
}
