package com.abc.itunes.controllers;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.abc.itunes.R;
import com.abc.itunes.adapters.AppsAdapter;
import com.abc.itunes.common.AppSession;
import com.abc.itunes.gui.RecyclerItemClickListener;
import com.abc.itunes.models.App;

/**
 * Created by Alberto on 20/07/17.
 */

public class ListFragment extends Fragment {

    private RecyclerView appsList;
    private LinearLayout reloadLyt;

    private AppsAdapter appsAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_list_layout, container, false);

        relationUI(rootView);

        initFragment();

        return rootView;
    }

    private void relationUI(View rootView) {
        appsList = (RecyclerView) rootView.findViewById(R.id.appsList);

        reloadLyt = (LinearLayout) rootView.findViewById(R.id.reloadLyt);
        reloadLyt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeController) getActivity()).getApps();
            }
        });
    }

    private void initFragment() {
        ((HomeController) getActivity()).getApps();
    }

    private void initList() {
        appsAdapter = new AppsAdapter(getActivity(), AppSession.appStore.getApps());

        LinearLayoutManager workoutsLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        appsList.setLayoutManager(workoutsLayoutManager);
        appsList.setAdapter(appsAdapter);

        appsList.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(),
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        ((HomeController) getActivity()).showAppDetail(AppSession.appStore.getApps().get(position));
                    }
                }));
    }

    public void showApps() {
        appsList.setVisibility(View.VISIBLE);
        reloadLyt.setVisibility(View.GONE);

        initList();
    }
}
