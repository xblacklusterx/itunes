package com.abc.itunes.controllers;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.abc.itunes.R;
import com.abc.itunes.common.Constants;
import com.abc.itunes.delegates.HomeDelegate;
import com.abc.itunes.models.App;
import com.afollestad.materialdialogs.MaterialDialog;

public class HomeController extends AppCompatActivity {

    private MaterialDialog materialProgress;

    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;

    private ListFragment listFragment;
    private DetailFragment detailFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmentManager = getSupportFragmentManager();

        showListFragment();
    }

    public void getApps() {
        showMaterialProgress(Constants._STR_LOADING, Constants._STR_WAIT);
        new HomeDelegate(this).getApps();
    }

    public void onSuccessAppsRequest() {
        hideProgressDialog();

        if (listFragment != null) {
            listFragment.showApps();
        }
    }

    public void onErrorAppsRequest(String errorMessage) {
        hideProgressDialog();

        showMaterialAlert(Constants._STR_TITLE_ERROR, errorMessage);
    }

    private void showListFragment() {
        listFragment = new ListFragment();

        showFragment(listFragment, "listFragment", "ListFragment");
    }

    private void showFragment(Fragment activeFragment, String fragmentTag, String fragmentClass) {
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, activeFragment, fragmentClass);
        fragmentTransaction.addToBackStack(fragmentTag);
        fragmentTransaction.commit();
    }

    private void showMaterialProgress(String title, String message) {
        materialProgress = new MaterialDialog.Builder(this)
                .backgroundColor(ContextCompat.getColor(this, R.color.white))
                .title(title)
                .titleColor(ContextCompat.getColor(this, R.color.colorPrimary))
                .content(message)
                .contentColor(ContextCompat.getColor(this, R.color.black))
                .progress(true, 0)
                .progressIndeterminateStyle(false)
                .build();
        materialProgress.setCancelable(false);
        materialProgress.show();
    }

    private void hideProgressDialog() {
        if (materialProgress != null) {
            materialProgress.dismiss();
        }
    }

    public void showMaterialAlert(String title, String content) {
        new MaterialDialog.Builder(this)
                .backgroundColor(ContextCompat.getColor(this, R.color.white))
                .title(title)
                .titleColor(ContextCompat.getColor(this, R.color.colorPrimary))
                .content(content)
                .contentColor(ContextCompat.getColor(this, R.color.black))
                .positiveText("Aceptar")
                .show();
    }

    public void showAppDetail(App selectedApp) {
        detailFragment = new DetailFragment();

        Bundle detailArguments = new Bundle();
        detailArguments.putString("appName", selectedApp.getAppName());
        detailArguments.putString("appPrice", selectedApp.getAppPrice());
        detailArguments.putString("appSummary", selectedApp.getAppSummary());
        detailArguments.putString("imageUrl", selectedApp.getAppImage());

        detailFragment.setArguments(detailArguments);

        showFragment(detailFragment, "detailFragment", "DetailFragment");
    }
}
