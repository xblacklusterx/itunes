package com.abc.itunes.controllers;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.abc.itunes.R;
import com.abc.itunes.common.AppSession;
import com.abc.itunes.common.Constants;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

/**
 * Created by Alberto on 20/07/17.
 */

public class DetailFragment extends Fragment {

    private ImageView appImage;
    private TextView appNameTxt;
    private TextView appPriceTxt;
    private TextView appSummaryTxt;

    private String imageUrl;
    private String appName;
    private String appPrice;
    private String appSummary;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        imageUrl = getArguments().getString("imageUrl");
        appName = getArguments().getString("appName");
        appPrice = getArguments().getString("appPrice");
        appSummary = getArguments().getString("appSummary");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_detail_layout, container, false);

        relationUI(rootView);

        initFragment();

        return rootView;
    }

    private void relationUI(View rootView) {
        appImage = (ImageView) rootView.findViewById(R.id.appImage);
        appNameTxt = (TextView) rootView.findViewById(R.id.appDetailNameTxt);
        appPriceTxt = (TextView) rootView.findViewById(R.id.appDetailPriceTxt);
        appSummaryTxt = (TextView) rootView.findViewById(R.id.appSummaryTxt);
    }

    private void initFragment() {
        Glide.with(getActivity()).load(imageUrl).into(appImage);
        appNameTxt.setText(appName);
        appPriceTxt.setText(Constants._CURRENCY + appPrice);
        appSummaryTxt.setText(appSummary);
    }
}
