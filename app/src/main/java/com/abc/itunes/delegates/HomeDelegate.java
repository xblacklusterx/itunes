package com.abc.itunes.delegates;

import com.abc.itunes.common.AppSession;
import com.abc.itunes.common.Constants;
import com.abc.itunes.controllers.HomeController;
import com.abc.itunes.io.BasicRunnable;
import com.abc.itunes.io.Server;
import com.abc.itunes.models.AppStore;
import com.abc.itunes.models.ServerResponse;

import static com.abc.itunes.common.Constants.StatusResponse.Ok;

/**
 * Created by Alberto on 20/07/17.
 */

public class HomeDelegate {

    private HomeController homeController;

    private HomeDelegate() {

    }

    public HomeDelegate(HomeController homeController) {
        this.homeController = homeController;
    }

    public void getApps() {
        Server.getInstance().doNetWorkOperation(new AppsRunnable());
    }

    class AppsRunnable extends BasicRunnable {

        @Override
        public void run() {
            ServerResponse serverResponse = getReturnObject();

            if (serverResponse != null) {
                switch (serverResponse.getStatus()) {
                    case Ok:
                        AppSession.appStore = (AppStore) serverResponse.getBean();
                        homeController.onSuccessAppsRequest();
                        break;

                    case Offline:
                        homeController.onErrorAppsRequest(Constants._STR_ERROR_NETWORK);
                        break;

                    default:
                        homeController.onErrorAppsRequest(Constants._STR_ERROR_SERVER);
                        break;
                }
            }
            else {
                homeController.onErrorAppsRequest(Constants._STR_ERROR_SERVER);
            }
        }
    }
}
