package com.abc.itunes.io;

import android.os.Handler;

import com.abc.itunes.common.Constants;
import com.abc.itunes.common.Utils;
import com.abc.itunes.models.ServerResponse;

import static com.abc.itunes.common.Constants.StatusResponse.Offline;
import static com.abc.itunes.common.Constants.StatusResponse.Ok;
import static com.abc.itunes.common.Constants.StatusResponse.Error;

/**
 * Created by Alberto on 20/07/17.
 */

public class Server {

    private static Server INSTANCE = null;

    private Handler myHandler = null;

    private Server() {
        myHandler = new Handler();
    }

    private synchronized static void createInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Server();
        }
    }

    public static Server getInstance() {
        if (INSTANCE == null) {
            createInstance();
        }
        return INSTANCE;
    }

    public void doNetWorkOperation(BasicRunnable action) {
        new AppsTask(action).start();
    }

    class AppsTask extends BasicThread {

        public AppsTask(Runnable runnable) {
            super(runnable);
        }

        @Override
        public void run() {

            if (Utils.isNetworkAvailable()) {

                HTTPInvoker httpInvoker = new HTTPInvoker();
                serverResponse = httpInvoker.sendJsonToUrl(Constants._URL);

                if (serverResponse != null) {
                    if (serverResponse.getStatus() == Ok) {
                        serverResponse = new JSONParser().parseAppsResponse(serverResponse);
                    } else {
                        serverResponse.setStatus(Error);
                    }
                }

            }
            else {
                serverResponse = new ServerResponse();
                serverResponse.setStatus(Offline);
            }

            myRunnable.setReturnObject(serverResponse);
            myHandler.post(myRunnable);
        }

    }

}
