package com.abc.itunes.io;

import com.abc.itunes.models.ServerResponse;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.abc.itunes.common.Constants.StatusResponse.Ok;
import static com.abc.itunes.common.Constants.StatusResponse.Error;

/**
 * Created by Alberto on 20/07/17.
 */

public class HTTPInvoker {

    public ServerResponse sendJsonToUrl(String postUrl)  {

        ServerResponse serverResponse = new ServerResponse();

        try {
            URL url = new URL(postUrl);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.setConnectTimeout(30000);
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);

            int responseCode = urlConnection.getResponseCode();

            if(responseCode == 200) {

                serverResponse.setStatus(Ok);

                BufferedReader in = new BufferedReader(
                        new InputStreamReader(urlConnection.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                in.close();

                serverResponse.setResponseString(response.toString());
            }
            else {
                serverResponse.setStatus(Error);
            }
        }
        catch (Exception e){
            serverResponse.setStatus(Error);
            e.printStackTrace();
        }

        return serverResponse;
    }

}
