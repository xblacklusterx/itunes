package com.abc.itunes.io;

import com.abc.itunes.models.ServerResponse;

/**
 * Created by Alberto on 20/07/17.
 */

public class BasicRunnable implements Runnable {

    private ServerResponse serverResponse;

    public ServerResponse getReturnObject() {
        return serverResponse;
    }

    public void setReturnObject(ServerResponse serverResponse) {
        this.serverResponse = serverResponse;
    }

    @Override
    public void run() {

    }
}
