package com.abc.itunes.io;

import com.abc.itunes.models.ServerResponse;

/**
 * Created by Alberto on 20/07/17.
 */

public class BasicThread extends Thread {

    public BasicRunnable myRunnable = null;
    public ServerResponse serverResponse = null;

    public BasicThread(Runnable runnable) {
        this.myRunnable = (BasicRunnable) runnable;
    }

}
