package com.abc.itunes.io;

import com.abc.itunes.common.Constants;
import com.abc.itunes.models.App;
import com.abc.itunes.models.AppStore;
import com.abc.itunes.models.ServerResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.abc.itunes.common.Constants.StatusResponse.Error;
import static com.abc.itunes.common.Constants.StatusResponse.Null;
import static com.abc.itunes.common.Constants.StatusResponse.Ok;
import static com.abc.itunes.common.Constants.StatusResponse.JSONException;

/**
 * Created by Alberto on 20/07/17.
 */

public class JSONParser {

    public ServerResponse parseAppsResponse(ServerResponse serverResponse) {

        if (serverResponse != null) {
            if (serverResponse.getStatus().equals(Ok)) {
                String responseString = serverResponse.getResponseString();
                if (responseString != null && responseString.length() > 0) {
                    try {
                        AppStore appStore = new AppStore();
                        ArrayList<App> appsList = new ArrayList<>();
                        JSONObject appsJson = new JSONObject(responseString);
                        JSONObject feedJson = appsJson.getJSONObject(Constants._JSON_KEY_FEED);
                        JSONArray entrysArray = feedJson.getJSONArray(Constants._JSON_KEY_ENTRY);

                        for (int i = 0; i < entrysArray.length(); i++) {
                            JSONObject entryJson = entrysArray.getJSONObject(i);
                            App currentApp = new App();

                            JSONObject nameJson = entryJson.getJSONObject(Constants._JSON_KEY_NAME);
                            currentApp.setAppName(nameJson.getString(Constants._JSON_KEY_LABEL));

                            JSONArray imagesJson = entryJson.getJSONArray(Constants._JSON_KEY_IMAGE);
                            for (int j = 0; j < imagesJson.length(); j++) {
                                JSONObject imageJson = imagesJson.getJSONObject(j);
                                String imageUrl = imageJson.getString(Constants._JSON_KEY_LABEL);
                                if (j == 0) {
                                    currentApp.setAppThumbnail(imageUrl);
                                }
                                else if (j == 2) {
                                    currentApp.setAppImage(imageUrl);
                                }
                            }

                            JSONObject summaryJson = entryJson.getJSONObject(Constants._JSON_KEY_SUMMARY);
                            currentApp.setAppSummary(summaryJson.getString(Constants._JSON_KEY_LABEL));

                            JSONObject priceJson = entryJson.getJSONObject(Constants._JSON_KEY_PRICE);
                            JSONObject attribsJson = priceJson.getJSONObject(Constants._JSON_KEY_ATTRIBUTES);
                            currentApp.setAppPrice(attribsJson.getString(Constants._JSON_KEY_AMOUNT));

                            appsList.add(currentApp);
                        }

                        appStore.setApps(appsList);
                        serverResponse.setBean(appStore);

                    }
                    catch (JSONException e) {
                        serverResponse.setStatus(JSONException);
                        e.printStackTrace();
                    }
                }
                else {
                    serverResponse.setStatus(Null);
                }
            }
            else {
                serverResponse.setStatus(Error);
            }
        }
        else {
            serverResponse.setStatus(Null);
        }

        return serverResponse;
    }

}
