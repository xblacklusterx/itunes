package com.abc.itunes.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.abc.itunes.R;
import com.abc.itunes.common.Constants;
import com.abc.itunes.models.App;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

/**
 * Created by Alberto on 20/07/17.
 */

public class AppsAdapter extends RecyclerView.Adapter<AppsAdapter.ViewHolder> {

    private Context context;
    private ArrayList<App> apps;

    public AppsAdapter(Context context, ArrayList<App> apps) {
        this.context = context;
        this.apps = apps;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView appThumbnailImg;
        public TextView appNameTxt;
        public TextView appPriceTxt;

        public ViewHolder(View view) {
            super(view);

            appThumbnailImg = (ImageView) view.findViewById(R.id.appThumbnailImg);

            appNameTxt = (TextView) view.findViewById(R.id.appNameTxt);
            appPriceTxt = (TextView) view.findViewById(R.id.appPriceTxt);
        }
    }

    @Override
    public AppsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_app_layout, parent, false);

        return new AppsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AppsAdapter.ViewHolder holder, int position) {
        App currentApp = apps.get(position);

        holder.appNameTxt.setText(currentApp.getAppName());
        holder.appPriceTxt.setText(Constants._CURRENCY + currentApp.getAppPrice());
        Glide.with(context).load(currentApp.getAppImage()).into(holder.appThumbnailImg);
    }

    @Override
    public int getItemCount() {
        return apps.size();
    }
}
