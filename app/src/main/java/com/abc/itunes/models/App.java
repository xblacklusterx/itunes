package com.abc.itunes.models;

/**
 * Created by Alberto on 20/07/17.
 */

public class App {

    private String appName;
    private String appImage;
    private String appSummary;
    private String appPrice;

    private String appThumbnail;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppImage() {
        return appImage;
    }

    public void setAppImage(String appImage) {
        this.appImage = appImage;
    }

    public String getAppSummary() {
        return appSummary;
    }

    public void setAppSummary(String appSummary) {
        this.appSummary = appSummary;
    }

    public String getAppPrice() {
        return appPrice;
    }

    public void setAppPrice(String appPrice) {
        this.appPrice = appPrice;
    }

    public String getAppThumbnail() {
        return appThumbnail;
    }

    public void setAppThumbnail(String appThumbnail) {
        this.appThumbnail = appThumbnail;
    }
}
