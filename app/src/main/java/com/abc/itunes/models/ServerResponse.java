package com.abc.itunes.models;

import com.abc.itunes.common.Constants.StatusResponse;

/**
 * Created by Alberto on 20/07/17.
 */

public class ServerResponse {

    private StatusResponse status;
    private GenericModel bean;
    private String responseString;
    private String responseCode;
    private String responseMessage;

    public StatusResponse getStatus() {
        return status;
    }

    public void setStatus(StatusResponse status) {
        this.status = status;
    }

    public GenericModel getBean() {
        return bean;
    }

    public void setBean(GenericModel bean) {
        this.bean = bean;
    }

    public String getResponseString() {
        return responseString;
    }

    public void setResponseString(String responseString) {
        this.responseString = responseString;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }
    
}
