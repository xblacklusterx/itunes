package com.abc.itunes.models;

import java.util.ArrayList;

/**
 * Created by Alberto on 20/07/17.
 */

public class AppStore extends GenericModel {

    private ArrayList<App> apps;

    public ArrayList<App> getApps() {
        return apps;
    }

    public void setApps(ArrayList<App> apps) {
        this.apps = apps;
    }
}
